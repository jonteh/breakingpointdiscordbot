﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.WebSocket;

using BPDiscordBot.Commands.Interface;
using BPDiscordBot.Commands.Actions;
using BPDiscordBot.Database;

namespace BPDiscordBot.Commands
{
    public class CommandHandler
    {
        private Dictionary<string, CommandAction> _commands;
        private IDatabase _database;

        public CommandHandler(IDatabase Database)
        {
            _database = Database;

            _commands = new Dictionary<string, CommandAction>();
            _commands.Add("commands", new CommandList());
            _commands.Add("topkillers", new TopKillers());

            Console.WriteLine("Command handler initialized with " + _commands.Count + " command actions.");
        }

        public async Task HandleCommand(string Command, SocketMessage Message)
        {
            string[] Params = Command.Split(' ');
            string Executor = Params[0].Remove(0, 1).ToLower();

            if (_commands.ContainsKey(Executor))
                await _commands[Executor].Execute(Executor, Params, Message, this, _database);
            else
                await _commands["commands"].Execute(Executor, Params, Message, this, _database);
        }

        private string Merge(string[] Params, int Start)
        {
            var Merged = new StringBuilder();
            for (int i = Start; i < Params.Length; i++)
            {
                if (i > Start)
                    Merged.Append(" ");
                Merged.Append(Params[i]);
            }

            return Merged.ToString();
        }

        public Dictionary<string, CommandAction> AllCommands
        {
            get { return _commands; }
        }

        public IDatabase GetDatabase
        {
            get { return _database; }
        }
    }    
}

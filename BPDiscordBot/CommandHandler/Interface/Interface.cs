﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.WebSocket;

using BPDiscordBot.Database;

namespace BPDiscordBot.Commands.Interface
{
    public interface CommandAction
    {
        Task Execute(string Command, string[] Params, SocketMessage Message, CommandHandler Handler, IDatabase Database);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.WebSocket;

using BPDiscordBot.Commands.Interface;
using BPDiscordBot.Database;

namespace BPDiscordBot.Commands.Actions
{
    class TopKillers : CommandAction
    {
        public async Task Execute(string Command, string[] Params, SocketMessage Message, CommandHandler Handler, IDatabase Database)
        {
            StringBuilder Builder = new StringBuilder();

            Builder.Append("```");
            Builder.Append("Top Killers:\n");
            Builder.Append("----------------------------\n");

            var GetKillers = Database.RunQuickWithResult("SELECT name,total_survivor_kills FROM profile WHERE total_survivor_kills > 0 ORDER BY total_survivor_kills DESC LIMIT 5");

            if (GetKillers.Count > 0)
            {
                int i = 1;

                foreach (var Row in GetKillers.Values)
                {
                    Builder.Append(i + ". " + Row["name"].ToString() + " with " + Row["total_survivor_kills"].ToString() + " kills\n");
                    i++;
                }

                Builder.Append("```");
                await Message.Channel.SendMessageAsync(Builder.ToString());
            }
            else
                await Message.Channel.SendMessageAsync("```There are no players on this hive with any kills.```");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.WebSocket;

using BPDiscordBot.Commands.Interface;
using BPDiscordBot.Database;

namespace BPDiscordBot.Commands.Actions
{
    class CommandList : CommandAction
    {
        public async Task Execute(string Command, string[] Params, SocketMessage Message, CommandHandler Handler, IDatabase Database)
        {
            StringBuilder Builder = new StringBuilder();

            Builder.Append("```");
            Builder.Append("Valid commands:\n");
            Builder.Append("----------------------------\n");
            foreach (string Value in Handler.AllCommands.Keys)
            {
                Builder.Append("!" + Value + "\n");
            }
            Builder.Append("```");

            await Message.Channel.SendMessageAsync(Builder.ToString());
        }
    }
}

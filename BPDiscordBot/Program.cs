﻿using Discord;
using Discord.WebSocket;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;

using BPDiscordBot.Database;
using BPDiscordBot.Commands;

namespace BPDiscordBot
{
    public class Program
    {
        private DiscordSocketClient _client;
        private IDatabase _database;
        private Dictionary<string, string> _config;
        private CommandHandler _handler;

        public static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            // TODO move to config.ini & read on startup
            _config = new Dictionary<string, string>();
            _config.Add("DBHost", "127.0.0.1");
            _config.Add("DBPort", "3306");
            _config.Add("DBUser", "bp");
            _config.Add("DBPass", "lol123");
            _config.Add("DBName", "bphive");
            _config.Add("BotToken", "");

            _database = new IDatabase(_config["DBHost"], _config["DBPort"], _config["DBUser"], _config["DBPass"], _config["DBName"]);

            if (_database.GetConnection() != null)
                Console.WriteLine("Database connection successful");

            _handler = new CommandHandler(_database);

            _client = new DiscordSocketClient();
            _client.MessageReceived += MessageReceived;

            await _client.LoginAsync(TokenType.Bot, _config["BotToken"]);
            await _client.StartAsync();
            await Task.Delay(-1);
        }

        private async Task MessageReceived(SocketMessage message)
        {
            string Content = message.Content;

            if (Content.StartsWith("!"))
                await _handler.HandleCommand(Content, message);
        }
    }
}